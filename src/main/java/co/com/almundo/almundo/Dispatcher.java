package co.com.almundo.almundo;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import co.com.almundo.almundo.dto.ActualCall;
import co.com.almundo.almundo.utils.EmployeeType;

/**
 * Class to dispatch the calls at the employees
 * 
 * @author USUARIO
 *
 */
public class Dispatcher {
	private final static Logger LOGGER = Logger.getLogger("co.com.almundo.almundo.Dispatcher");
	private LinkedBlockingQueue<ActualCall> callsOperator;
	private LinkedBlockingQueue<ActualCall> callsSupervisor;
	private LinkedBlockingQueue<ActualCall> callsDirector;

	/**
	 * Method to define the total of employees by type. Total 10
	 * 
	 * @param employees
	 */
	private void initializeCalls(ArrayList<Integer> employees) {
		callsOperator = new LinkedBlockingQueue<ActualCall>(employees.get(0));
		callsSupervisor = new LinkedBlockingQueue<ActualCall>(employees.get(1));
		callsDirector = new LinkedBlockingQueue<ActualCall>(employees.get(2));
	}
	
	/**
	 * Method to take the call by employees
	 * 
	 * @param employeeType
	 */
	public void takeCall(EmployeeType employeeType) {
		switch (employeeType) {
		case OPERATOR:
			LOGGER.log(Level.INFO, employeeType + " take the " + callsOperator.poll() + " call");
			break;
		case SUPERVISOR:
			LOGGER.log(Level.INFO, employeeType + " take the " + callsSupervisor.poll() + " call");
		case DIRECTOR:
			LOGGER.log(Level.INFO, employeeType + " take the " + callsDirector.poll() + " call");
		}

	}

	/**
	 * Central method to redirect the call at available employees (In order operator
	 * -> supervisor -> director)
	 * 
	 * @param call
	 * @param employees
	 */
	public void dispatchCall(ActualCall call, ArrayList<Integer> employees) {
		try {
			initializeCalls(employees);
			LOGGER.log(Level.INFO, "The first option to receive the call is the operator");
			callsOperator.add(call);
		} catch (Exception e1) {
			try {
				LOGGER.log(Level.INFO, "The second option to receive the call is the supervisor");
				callsSupervisor.add(call);
			} catch (Exception e2) {
				try {
					LOGGER.log(Level.INFO, "The third option to receive the call is the director");
					callsDirector.add(call);
				} catch (Exception e3) {
					LOGGER.log(Level.WARNING, "No employees available to take the call");
				}
			}
		}
	}
}
