package co.com.almundo.almundo;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import co.com.almundo.almundo.utils.DispatcherUtil;
import co.com.almundo.almundo.utils.EmployeeType;


public class TakeCall implements Runnable {
	
	private final static Logger LOGGER = Logger.getLogger("co.com.almundo.almundo.TakeCall");
	
	// central class
	private Dispatcher dispatcher;
	
	// enum emplyeType
	private EmployeeType employeeType;

	/**
	 * Constructor to initialize variables
	 * 
	 * @param dispatcher
	 * @param employeeType
	 */
	public TakeCall(Dispatcher dispatcher, EmployeeType employeeType) {
		this.dispatcher = dispatcher;
		this.employeeType = employeeType;
	}

	/**
	 * Execute TakeCall method
	 */
	public void run() {
		try {
			while (Boolean.TRUE) {
				dispatcher.takeCall(employeeType);
				TimeUnit.SECONDS.sleep(DispatcherUtil.randomTime());				
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Not available");
		}
	}
}
