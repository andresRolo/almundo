package co.com.almundo.almundo.dto;

/**
 * Class to represent the call Object in the project
 * @author USUARIO
 *
 */
public class ActualCall {
	@SuppressWarnings("unused")
	private int id;

	public ActualCall(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
