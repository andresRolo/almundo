package co.com.almundo.almundo.utils;

import java.util.Random;

/**
 * Utility class
 * @author USUARIO
 *
 */
public class DispatcherUtil {

	//min call duration in seconds
	private static int min = 5;
	//max call duration in seconds
	private static int max = 10;
	
	/**
	 * Method to generate the random time
	 * @return int
	 */
	public static int randomTime() {
		Random random = new Random();
		return random.nextInt(max-min)+ min;
	}
}
