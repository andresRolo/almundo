package co.com.almundo.almundo.utils;

/**
 * Enum utility to define the employee type
 * @author USUARIO
 *
 */
public enum EmployeeType {
	//Definition of all employee types
	OPERATOR(1), SUPERVISOR(2), DIRECTOR(3);

	private int value;

	private EmployeeType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
