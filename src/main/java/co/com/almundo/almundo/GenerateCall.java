package co.com.almundo.almundo;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import co.com.almundo.almundo.dto.ActualCall;

/**
 * Class to generate the call with threads
 * 
 * @author USUARIO
 *
 */
public class GenerateCall implements Runnable {
	private final static Logger LOGGER = Logger.getLogger("co.com.almundo.almundo.GenerateCall");

	// central class
	private Dispatcher dispatcher;

	// actual call
	private ActualCall call;

	// total employees by type
	private ArrayList<Integer> employees;

	/**
	 * Constructor to initialize variables
	 * 
	 * @param call
	 * @param dispatcher
	 */
	public GenerateCall(ActualCall call, Dispatcher dispatcher, ArrayList<Integer> employees) {
		this.call = call;
		this.dispatcher = dispatcher;
		this.employees = employees;
	}

	/**
	 * Execute GenerateCall method
	 */
	public void run() {
		try {
			dispatcher.dispatchCall(call, employees);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Not available");
		}
	}

}
