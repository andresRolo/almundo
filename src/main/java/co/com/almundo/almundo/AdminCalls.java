package co.com.almundo.almundo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import co.com.almundo.almundo.dto.ActualCall;
import co.com.almundo.almundo.utils.EmployeeType;

/**
 * Class to launch all the operation
 * @author USUARIO
 *
 */
public class AdminCalls {
	private final static Logger LOGGER = Logger.getLogger("co.com.almundo.almundo.AdminCalls");
	//Thread to configure the calls
	private ExecutorService threadCalls;
	//Thread to configure the employees
	private ExecutorService threadEmployees;
	//Total of employees by type
	private ArrayList<Integer> employees;
	//Central class
	private Dispatcher dispatcher;
	//Types of all employees
	private List<EmployeeType> employeeType;
	
	/**
	 * Constructor to initialize variables
	 * 
	 * @param threadCalls
	 * @param threadEmployees
	 * @param dispatcher
	 * @param employees
	 * @param employeeType
	 */
	public AdminCalls(ExecutorService threadCalls, ExecutorService threadEmployees, Dispatcher dispatcher, ArrayList<Integer> employees, List<EmployeeType> employeeType) {
		this.threadCalls = threadCalls;
		this.threadEmployees = threadEmployees;
		this.dispatcher = dispatcher;
		this.employees = employees;
		this.employeeType = employeeType;
	}

	/**
	 * Method to start the admon calls
	 * @param totalCalls
	 */
	public void launchAdmonCalls(int totalCalls) {
		try {
			LOGGER.log(Level.INFO, "launchAdmonCalls");
			executeThreadEmployee();
			for (int i = 1; i <= totalCalls; i++) {
				executeThreadCall(i);
			}
		} catch(Exception ex) {
			LOGGER.log(Level.SEVERE, "exception in launchAdmonCalls " + ex.getMessage());
		}
	}

	/**
	 * 
	 */
	public void executeThreadEmployee() {
		for (EmployeeType type : employeeType) {
			TakeCall takeCall = new TakeCall(dispatcher, type);
			threadEmployees.execute(takeCall);
		}
	}

	/**
	 * 
	 * @param call
	 */
	public void executeThreadCall(int call) {
		GenerateCall generateCall = new GenerateCall(new ActualCall(call), dispatcher, employees);
		threadCalls.execute(generateCall);
	}

}
