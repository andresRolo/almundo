package co.com.almundo.almundo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Before;
import org.junit.Test;
import co.com.almundo.almundo.utils.EmployeeType;

public class AdminCallsTest {
		private ExecutorService threadCalls;
		private ExecutorService threadEmployees;
		private ArrayList<Integer> employees;
		private Dispatcher dispatcher;
		private List<EmployeeType> employeeType;
		private AdminCalls adminCalls;
		
		/**
		 * Default configurations and variables definitions
		 */
		@Before
		public void setUp() {
			threadCalls = Executors.newFixedThreadPool(10);
			dispatcher = new Dispatcher();
			threadEmployees = Executors.newFixedThreadPool(10);
			employeeType = buildEmployeeTypes();
			employees = buildTotalEmployees();
			adminCalls = new AdminCalls(threadCalls, threadEmployees, dispatcher, employees, employeeType);
		}

		/**
		 * Define total employees by type
		 * @return
		 */
		private ArrayList<Integer> buildTotalEmployees() {
			ArrayList<Integer> employees = new ArrayList<Integer>();
			employees.add(8);
			employees.add(1);
			employees.add(1);
			return employees;
		}

		/**
		 * Define all employees type
		 * @return
		 */
		private List<EmployeeType> buildEmployeeTypes() {
			List<EmployeeType> type = new ArrayList<EmployeeType>();
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.OPERATOR);
			type.add(EmployeeType.SUPERVISOR);
			type.add(EmployeeType.DIRECTOR);
			return type;
		}
		
		/**
		 * Lauch test
		 */
		@Test
		public void startOperationTest() {
			adminCalls.launchAdmonCalls(10);
		}
}
